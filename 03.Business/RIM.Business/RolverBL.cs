﻿using RIM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RIM.Entities.Concrete;
using RIM.Data;

namespace RIM.Business
{
    public class RolverBL : IRover
    {
        /// <summary>
        /// Get the Rover Result entity
        /// </summary>
        /// <param name="Rover">Rover entity to process</param>
        /// <returns></returns>
        public RoverResultEntity ProcessRover(RoverEntity Rover)
        {
            RoverData roverData = new RoverData();

            return roverData.ProcessRover(Rover);
        }
    }
}
