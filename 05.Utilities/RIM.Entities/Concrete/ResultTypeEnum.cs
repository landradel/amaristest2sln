﻿using RIM.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIM.Entities.Concrete
{
    public enum ResultTypeEnum
    {
        [StringValue("The command has been proccesed successfully")]
        Success = 0,

        [StringValue("The command has an invalid format. Please use only the letters: A - L - R")]
        InvalidCommand = 1,

        [StringValue("The command instructions leave the Rover is out of the stage range")]
        CommandOutOfStage = 2
    }

}
