﻿using RIM.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIM.Entities.Concrete
{
    public class RoverResultEntity : Coordinate
    {
        #region [CONSTRUCTORS]

        /// <summary>
        /// Empty constructor
        /// </summary>
        public RoverResultEntity()
        {
            commands = new Dictionary<int, string>();
        }

        /// <summary>
        /// Set the values of the RoverResult entity
        /// </summary>
        /// <param name="Result">Boolean result</param>
        public RoverResultEntity(bool Result)
        {
            result = Result;
            commands = new Dictionary<int, string>();
        }

        /// <summary>
        /// Set the values of the RoverResult entity
        /// </summary>
        /// <param name="Message">Message result</param>
        public RoverResultEntity(string Message)
        {
            message = Message;
            commands = new Dictionary<int, string>();
        }

        /// <summary>
        /// Set the values of the RoverResult entity
        /// </summary>
        /// <param name="Result">Boolean result</param>
        /// <param name="Message">Message result</param>
        public RoverResultEntity(bool Result, string Message)
        {
            result = Result;
            message = Message;
            commands = new Dictionary<int, string>();
        }

        #endregion

        #region [PROPERTIES]

        private bool result;

        private string message;

        private Dictionary<int, string> commands;

        private ResultTypeEnum resultType;

        private string resultTypeString;

        private List<SpecialCoordinate> coordinates;

        private DirectionEnum actualDirection;

        private string actualDireectionString;

        public bool Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
            }
        }

        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }

        public ResultTypeEnum ResultType
        {
            get
            {
                return resultType;
            }

            set
            {
                resultType = value;
            }
        }

        public string ResultTypeString
        {
            get
            {
                return resultTypeString;
            }

            set
            {
                resultTypeString = value;
            }
        }

        public List<SpecialCoordinate> Coordinates
        {
            get
            {
                return coordinates;
            }

            set
            {
                coordinates = value;
            }
        }

        public DirectionEnum ActualDirection
        {
            get
            {
                return actualDirection;
            }

            set
            {
                actualDirection = value;
            }
        }

        public string ActualDireectionString
        {
            get
            {
                return actualDireectionString;
            }

            set
            {
                actualDireectionString = value;
            }
        }


        public void SetCommand(int index, string value)
        {
            commands.Add(index, value);
        }

        public int GetCommandLength()
        {
            return commands.Count;
        }

        public string GetCommandByIndex(int index)
        {
            string theCommand = "";
            commands.TryGetValue(index, out theCommand);
            return theCommand;

        }

        #endregion
    }
}
