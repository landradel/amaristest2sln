﻿using RIM.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIM.Entities.Concrete
{
    public class RoverEntity : Coordinate
    {
        #region [CONSTRUCTORS]

        /// <summary>
        /// Empty constructor
        /// </summary>
        public RoverEntity()
        {

        }

        /// <summary>
        /// Set the values of the Rover entity
        /// </summary>
        /// <param name="Command">Command to process</param>
        public RoverEntity(string Command)
        {

        }

        /// <summary>
        /// Set the values of the Rover entity
        /// </summary>
        /// <param name="XCoordinate">X coordinate</param>
        /// <param name="YCoordinate">Y coordinate</param>
        public RoverEntity(int XCoordinate, int YCoordinate) : base (XCoordinate, YCoordinate)
        {
            this.XCoordinate = XCoordinate;
            this.YCoordinate = YCoordinate;
        }

        /// <summary>
        /// Set the values of the Rover entity
        /// </summary>
        /// <param name="XCoordinate">X coordinate</param>
        /// <param name="YCoordinate">Y coordinate</param>
        /// <param name="StageWidth">Stage width</param>
        /// <param name="StageHeight">Stage height</param>
        public RoverEntity(int XCoordinate, int YCoordinate, int StageWidth, int StageHeight) : base(XCoordinate, YCoordinate)
        {
            this.XCoordinate = XCoordinate;
            this.YCoordinate = YCoordinate;
            stageWidth = StageWidth;
            stageHeight = StageHeight;
        }

        /// <summary>
        /// Set the values of the Rover entity
        /// </summary>
        /// <param name="Command">Command to process</param>
        /// <param name="XCoordinate">X coordinate</param>
        /// <param name="YCoordinate">Y coordinate</param>
        /// <param name="StageWidth">Stage width</param>
        /// <param name="StageHeight">Stage height</param>
        public RoverEntity(string Command, int XCoordinate, int YCoordinate) : base (XCoordinate, YCoordinate)
        {
            this.Command = Command;
            this.XCoordinate = XCoordinate;
            this.YCoordinate = YCoordinate;
        }

        /// <summary>
        /// Set the values of the Rover entity
        /// </summary>
        /// <param name="Command">Command to process</param>
        /// <param name="XCoordinate">X coordinate</param>
        /// <param name="YCoordinate">Y coordinate</param>
        public RoverEntity(string Command, int XCoordinate, int YCoordinate, int StageWidth, int StageHeight) : base(XCoordinate, YCoordinate)
        {
            this.Command = Command;
            this.XCoordinate = XCoordinate;
            this.YCoordinate = YCoordinate;
            stageWidth = StageWidth;
            stageHeight = StageHeight;
        }

        #endregion

        #region [PROPERTIES]

        private string command;

        private int stageHeight;

        private int stageWidth;

        public string Command
        {
            get
            {
                return command;
            }

            set
            {
                command = value;
            }
        }

        public int StageHeight
        {
            get
            {
                return stageHeight;
            }

            set
            {
                stageHeight = value;
            }
        }

        public int StageWidth
        {
            get
            {
                return stageWidth;
            }

            set
            {
                stageWidth = value;
            }
        }



        #endregion

    }
}
