﻿using RIM.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIM.Entities.Abstract
{
    public abstract class Coordinate
    {
        #region [CONSTRUCTORS]

        /// <summary>
        /// Create coordinate entity
        /// </summary>
        public Coordinate()
        {

        }

        /// <summary>
        /// Set the value of x and y coordinates
        /// </summary>
        /// <param name="XCoordinate"></param>
        /// <param name="YCoordinate"></param>
        public Coordinate(int XCoordinate, int YCoordinate)
        {
            this.xCoordinate = XCoordinate;
            this.yCoordinate = YCoordinate;
        }

        #endregion

        #region [PROPERTIES]

        private int xCoordinate;

        private int yCoordinate;

        private DirectionEnum actualDirection;

        private string actualDirectionString;

        public int XCoordinate
        {
            get
            {
                return xCoordinate;
            }

            set
            {
                xCoordinate = value;
            }
        }
        public int YCoordinate
        {
            get
            {
                return yCoordinate;
            }

            set
            {
                yCoordinate = value;
            }
        }

        public DirectionEnum ActualDirection
        {
            get
            {
                return actualDirection;
            }

            set
            {
                actualDirection = value;
            }
        }

        public string ActualDirectionString
        {
            get
            {
                return actualDirectionString;
            }

            set
            {
                actualDirectionString = value;
            }
        }

        #endregion

        #region [PUBLIC METHODS]

        /// <summary>
        /// Get the actual string coordinate in base (x,y)
        /// </summary>
        /// <returns></returns>
        public string GetCompleteCoordinate()
        {
            return "(" + this.xCoordinate + "," + this.yCoordinate + ")";
        }

        #endregion

        #region [PRIVATE METHODS]

        #endregion
    }
}
